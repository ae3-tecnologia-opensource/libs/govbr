<?php
/*
|--------------------------------------------------------------------------
| To publish the config files
|--------------------------------------------------------------------------
|
| Execute the command below do publish the config file
| php artisan vendor:publish --provider="Ae3\GovBr\app\Providers\GovBrServiceProvider" --tag="config"
*/

return [
    'oauth2' => [
        'client_id' => env('GOV_BR_CLIENT_ID'),
        'client_secret' => env('GOV_BR_CLIENT_SECRET'),
        'redirect' => env('GOV_BR_REDIRECT_URI'),
        'scopes' => env('GOV_BR_SCOPES', 'govbr_empresa,phone,email,openid,govbr_confiabilidades,profile'),
        'enabled' => env('GOV_BR_ENABLED', true),
        'decode_url' => env('GOV_URL_DECODE', false)
    ],
    'environments' => [
        /*
        |--------------------------------------------------------------------------
        | Default Environment
        |--------------------------------------------------------------------------
        |
        | Here you may specify the default environment that should be used
        | by the application. There are two possibilities: "staging" and "production"
        */
        'default' => env('GOV_BR_DEFAULT_ENVIRONMENT', 'staging'),
        'staging' => [
            'uri' => env('GOV_BR_STAGING_URI', 'https://sso.staging.acesso.gov.br'),
        ],
        'production' => [
            'uri' => env('GOV_BR_PRODUCTION_URI', 'https://sso.acesso.gov.br'),
        ]
    ]
];
