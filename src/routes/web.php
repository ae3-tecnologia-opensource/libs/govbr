<?php

use Illuminate\Support\Facades\Route;

use Ae3\GovBr\app\Http\Controllers\AuthController;

Route::prefix('integrations')
    ->middleware(['web'])
    ->group(function () {
        Route::prefix('govbr')->group(function () {
            Route::get('/redirect', [AuthController::class, 'redirect']);
            Route::get('/authenticated', [AuthController::class, 'authenticated']);
        });
    });
