<?php

namespace Ae3\GovBr\app\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function redirect(): RedirectResponse
    {
        return Socialite::driver('govbr')->redirect();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticated(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'data' => (array) Socialite::driver('govbr')->user()
        ]);
    }
}
