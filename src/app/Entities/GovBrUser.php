<?php

namespace Ae3\GovBr\app\Entities;

use Laravel\Socialite\Contracts\User;

class GovBrUser implements User
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $refreshToken;

    /**
     * @var string
     */
    protected $expiresIn;

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->getResponseValue('sub');
    }

    /**
     * @return mixed|null
     */
    public function getCpf()
    {
        return $this->getResponseValue('sub');
    }

    /**
     * @return mixed|null
     */
    public function getName()
    {
        return $this->getResponseValue('name');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $response = $this->response;
        $response['cpf'] = $this->getCpf();
        return $response;
    }

    /**
     * @return mixed|null
     */
    public function getEmail()
    {
        return $this->getResponseValue('email');
    }

    /**
     * @return bool
     */
    public function emailVerified(): bool
    {
        return (bool) $this->getResponseValue('email_verified');
    }

    /**
     * @return mixed|null
     */
    public function getPhoneNumber()
    {
        return $this->getResponseValue('phone_number');
    }

    /**
     * @return bool
     */
    public function phoneNumberVerified(): bool
    {
        return (bool) $this->getResponseValue('phone_number_verified');
    }

    /**
     * @return mixed|null
     */
    public function getAvatarUrl()
    {
        return $this->getResponseValue('picture');
    }

    /**
     * @return mixed|null
     */
    public function getProfile()
    {
        return $this->getResponseValue('profile');
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    private function getResponseValue(string $key)
    {
        return $this->response[$key] ?? null;
    }

    /**
     * @param string $token
     * @return GovBrUser
     */
    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @param string|null $refreshToken
     * @return GovBrUser
     */
    public function setRefreshToken(?string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * @param string $expiresIn
     * @return GovBrUser
     */
    public function setExpiresIn(string $expiresIn): self
    {
        $this->expiresIn = $expiresIn;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getNickname()
    {
        // TODO: Implement getNickname() method.
    }

    /**
     * @inheritDoc
     */
    public function getAvatar()
    {
        // TODO: Implement getAvatar() method.
    }
}
