<?php

namespace Ae3\GovBr\app\Providers;

use Ae3\GovBr\app\Entities\GovBrUser;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;

class GovBrSocialiteProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * @var string
     */
    private $codeVerifier;

    /**
     * @var string
     */
    private $codeChallenge;

    /**
     * @var string
     */
    private $codeChallengeMethod = 'S256';

    /**
     * @var string
     */
    private $nonce;

    /**
     * @var string
     */
    protected $urlAuthorize;

    /**
     * @var string
     */
    protected $urlAccessToken;

    /**
     * @var string
     */
    protected $urlResourceOwnerInfo;

    /**
     * @var string
     */
    protected $urlLogout;

    /**
     * @inheritDoc
     */
    protected $scopeSeparator = '+';

    /**
     * @param Request $request
     * @param $clientId
     * @param $clientSecret
     * @param $redirectUrl
     * @param $guzzle
     * @throws Exception
     */
    public function __construct(Request $request, $clientId, $clientSecret, $redirectUrl, $guzzle = [])
    {
        parent::__construct($request, $clientId, $clientSecret, $redirectUrl, $guzzle);
        list(
            $this->urlAuthorize,
            $this->urlAccessToken,
            $this->urlResourceOwnerInfo,
            $this->urlLogout
        ) = array_values(self::getEnvironment());
    }

    /**
     * @return void
     * @throws Exception
     */
    private function reset()
    {
        $this->generateCodeVerifier();
        $this->generateCodeChallenge();
        $this->generateNonce();
    }

    /**
     * @throws Exception
     */
    private function generateCodeVerifier()
    {
        $verifier_bytes = random_bytes(64);
        $this->codeVerifier = rtrim(strtr(base64_encode($verifier_bytes), "+/", "-_"), "=");
        $this->request->session()->put('code_verifier', $this->codeVerifier);
    }

    /**
     * @return void
     */
    private function generateCodeChallenge()
    {
        $challenge_bytes = hash("sha256", $this->codeVerifier, true);
        $this->codeChallenge = rtrim(strtr(base64_encode($challenge_bytes), "+/", "-_"), "=");
    }

    /**
     * @return void
     */
    private function generateNonce()
    {
        $this->nonce = md5(uniqid('govbr', true));
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getAuthUrl($state): string
    {
        $this->reset();

        $authUrl = $this
            ->scopes(explode(',', config('govbr.oauth2.scopes')))
            ->with([
                'nonce' => $this->nonce,
                'code_challenge' => $this->codeChallenge,
                'code_challenge_method' => $this->codeChallengeMethod
            ])
            ->buildAuthUrlFromBase($this->urlAuthorize, $state);

        if (config('govbr.oauth2.decode_url')){
            return urldecode($authUrl);
        }

        return $authUrl;
    }

    /**
     * @inheritDoc
     */
    protected function getTokenUrl(): string
    {
        return $this->urlAccessToken;
    }

    /**
     * Get the access token response for the given code.
     *
     * @param string $code
     * @return array
     * @throws GuzzleException
     */
    public function getAccessTokenResponse($code): array
    {
        $authorization = base64_encode("$this->clientId:$this->clientSecret");
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => "Basic $authorization"
            ],
            'form_params' => $this->getTokenFields($code),
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Get the POST fields for the token request.
     *
     * @param  string  $code
     * @return array
     */
    protected function getTokenFields($code): array
    {
        //TODO: Adicionar o grant_type ao config do pacote
        return [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $this->redirectUrl,
            //TODO:: Tratar possível code_verifier não encontrado na sessão
            'code_verifier' => $this->request->session()->pull('code_verifier'),
        ];
    }

    /**
     * @inheritDoc
     * @throws GuzzleException
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get($this->urlResourceOwnerInfo, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Authorization' => "Bearer $token",
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @inheritDoc
     */
    protected function mapUserToObject(array $user)
    {
        return new GovBrUser($user);
    }

    /**
     * @return string[]
     */
    final public static function getEnvironment(): array
    {
        $environment = config('govbr.environments.default');
        $uri = config("govbr.environments.{$environment}.uri");
        return [
            'urlAuthorize' => $uri . '/authorize',
            'urlAccessToken' => $uri . '/token',
            'urlResourceOwnerInfo' => $uri . '/userinfo',
            //TODO: Verificar necessidade de implementar logout
            'urlLogout' => $uri . '/logout',
        ];
    }
}
