<?php

namespace Ae3\GovBr\app\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\Contracts\Factory;

class GovBrServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'govbr');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../../config/config.php' => config_path('govbr.php'),
            ], 'config');
            $this->publishes([
                __DIR__.'/../../resources/views' => base_path('resources/views/vendor/govbr'),
            ], 'views');
            $this->publishes([
                __DIR__.'/../../resources/assets' => public_path('vendor/govbr'),
            ], 'assets');
        }

        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'govbr');

        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->bootSpotifySocialite();
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    private function bootSpotifySocialite()
    {
        $socialite = $this->app->make(Factory::class);
        $socialite->extend('govbr', function ($app) use ($socialite) {
            $config = $app['config']['govbr']['oauth2'];
            return $socialite->buildProvider(GovBrSocialiteProvider::class, $config);
        });
    }
}
