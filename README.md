## GovBr
Projeto de integração com o Login Único do GovBr (OAuth2) utilizando Laravel Socialite.

### Requisitos
- PHP >= v7.1
- Laravel >= v5.8
- Composer >= v2

### Como configurar o projeto?

1) Adicione este repositório à lista de repositórios do composer em seu projeto laravel.  
```json
{
  "repositories": [
    {
      "type": "git",
      "url": "https://gitlab.com/ae3-tecnologia-opensource/libs/govbr.git"
    }
  ]
}
```
2) Execute o comando a seguir para baixar esta lib ao vendor do seu projeto.  
```
composer require ae3/gov_br
```

3) Configure as variáveis abaixo no .env do seu projeto.  
```
GOV_BR_CLIENT_ID="<Seu client ID>"
GOV_BR_CLIENT_SECRET="<Seu Client Secret>"
GOV_BR_REDIRECT_URI="<Sua URL de redirecionamento>"
GOV_BR_DEFAULT_ENVIRONMENT="staging"
```
A variável GOV_BR_DEFAULT_ENVIRONMENT deve ter o valor "staging" para se autenticar com o ambiente de homologação e "production" para se autenticar com o ambiente de produção.

### Utilizando o componente button 
Para utilizar o botão de login, siga os passos a seguir:

1) Execute o comando abaixo para fazer a publicação dos assets na pasta pública do seu projeto. 
```shell
php artisan vendor:publish --provider="Ae3\GovBr\app\Providers\GovBrServiceProvider" --tag="assets"
```
 
2) Adicione a referência ao css "core.min.css" no layout da sua tela de login.
```html
<link href="{{ asset('vendor/govbr/css/core.min.css') }}" rel="stylesheet" type="text/css">
```

3) Inclua o botão no arquivo blade da página de login de sua aplicação:
```php
@include('govbr::button')
```
Após os passos acima um botão com o label "Entrar com GOV.BR" aparecerá na tela de login.

### Utilizando a URL de redirecionamento

Não documentado...